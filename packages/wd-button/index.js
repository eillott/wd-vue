import Button from './src/wd-button';

Button.install = function (Vue) {
  Vue.component(Button.name, Button);
};

export default Button;
