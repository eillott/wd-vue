import WDButton from './wd-button/index.js';
import WDTag from './wd-tag/index.js';
import WDDiscuss from './wd-discuss/index.js';

const components = [
  WDButton,
  WDTag,
  WDDiscuss
]

const install = function(Vue) {
  components.map(component => Vue.component(component.name, component))
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  WDButton,
  WDTag,
  WDDiscuss
}
