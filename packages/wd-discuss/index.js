import Discuss from './src/wd-discuss';

Discuss.install = function (Vue) {
  Vue.component(Discuss.name, Discuss);
};

export default Discuss;
