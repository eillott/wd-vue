import Vue from 'vue'
import Discuss from './index'

Discuss.install(Vue);
new Vue({
  el:"#discuss"
});

export default Vue;
