import { http, Query } from './utils'
import { LS_ACCESS_TOKEN_KEY, LS_USER_KEY, NOT_INITIALIZED_ERROR } from './constants'
import { giteehub as giteehubIcon } from '../giteement/icons'
import moment from 'moment';
const response_type = 'code'
const grant_type = 'authorization_code'
const LS_ACCESS_TOKEN_KEY_DAY = LS_ACCESS_TOKEN_KEY + moment().format('YYYYMMDD');

class Giteement{

  constructor(options = {}) {
    Object.assign(this, options);

    this.user = {
      isLogin: false,
      html_url: this.loginLink,
      avatar_url: giteehubIcon,
      fromCache: true
    };

    const query = Query.parse();
    if (query.code) {
      const { client_id, client_secret } = this.oauth;
      const redirect_uri = this.backcall_uri + '?bkurl='
        + encodeURIComponent(window.location.origin + window.location.pathname + window.location.hash.split("?")[0]);
      const code = query.code
      delete query.code
      const search = Query.stringify(query);
      const replacedUrl = `${window.location.origin}${window.location.pathname}${search}` + window.location.hash.split("?")[0];
      history.replaceState({}, '', replacedUrl);

      http.post(this.oauth_uri, {
        code,
        client_id,
        client_secret,
        redirect_uri,
        grant_type
      }, '').then(data => {
        localStorage.clear();
        this.accessToken = data.access_token;
        this.createUserInfo();
      });
    } else {
      this.createUserInfo();
    }
  }


  get accessToken() {
    let token = localStorage.getItem(LS_ACCESS_TOKEN_KEY_DAY);
    if (!token) {
      localStorage.clear();
    }
    return token
  }
  set accessToken(token) {
    localStorage.setItem(LS_ACCESS_TOKEN_KEY_DAY, token)
  }

  // get user() {
  //   let user = localStorage.getItem(LS_USER_KEY);
  //   if (!user) {
  //     localStorage.clear();
  //   }
  //   return JSON.parse(user);
  // }
  //
  // set user(user) {
  //   localStorage.setItem(LS_USER_KEY, JSON.stringify(user));
  // }

  get loginLink() {
    const oauthUri = 'https://gitee.com/oauth/authorize'
    const redirect_uri = this.backcall_uri + '?bkurl='  + encodeURIComponent(window.location.href);
    const oauthParams = Object.assign({
      response_type,
      redirect_uri,
    }, this.oauth);
    return `${oauthUri}${Query.stringify(oauthParams)}`
  }

  createUserInfo() {
    let curUser = {};
    try {
      if (this.accessToken) {
        const userInfo = localStorage.getItem(LS_USER_KEY);
        if (!userInfo) {
          http.get('/user', {
            token: this.accessToken
          })
            .then((userInfo) => {
              Object.assign(curUser, userInfo, {
                isLogin: true,
                fromCache: true
              });
              localStorage.setItem(LS_USER_KEY, JSON.stringify(curUser));
              this.user = curUser
            });
        } else {
          this.user = JSON.parse(userInfo);
        }
      } else {
        Object.assign(curUser,  {
          isLogin: false,
          html_url: this.loginLink,
          avatar_url: giteehubIcon,
          fromCache: true
        });
        this.user = curUser;
      }
    } catch (e) {
      console.error(e);
      localStorage.removeItem(LS_USER_KEY);
      localStorage.removeItem(LS_ACCESS_TOKEN_KEY);
    }
  }

  getIssue() {
    return this.loadMeta();
  }

  loadMeta() {
    const { id, owner, repo } = this;
    return http.get(`/repos/${owner}/${repo}/issues`, {
      creator: owner,
      labels: id,
    }).then(issues => {
      if (!issues.length) return Promise.reject(NOT_INITIALIZED_ERROR)
      return issues[0]
    })
  }

  post(body) {
    const { accessToken } = this
    return this.getIssue()
      .then(issue => http.post(issue.url + '/comments', { access_token: accessToken,body }, ''));
  }

  markdown(text) {
    return http.post('/markdown', {
      text
    })
  }

  loadComments() {
    return this.getIssue()
      .then(issue => http.get(issue.url + '/comments', { page: 1, per_page: 20 }, ''))
      .then((comments) => {
        return comments
      })
  }

  logout() {
    localStorage.clear();
    this.createUserInfo();
  }
}

export default Giteement;
