import Tag from './src/wd-tag'

Tag.install = function(Vue) {
  Vue.component(Tag.name, Tag)
}

export default Tag
