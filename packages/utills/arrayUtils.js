// 数组删除指定元素
export function ArrayDelete(arr, item) {
  let index = arr.indexOf(item);
  if (index > -1) {
    arr.splice(index, 1);
  }
}

// 从一个数组内删除另外一个数组
export function ArrayDeletArray (arr, delArr) {
  delArr.forEach(item => {
    ArrayDelete(arr, item);
  });
}
