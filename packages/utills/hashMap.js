

class HashMap {

  constructor(options = {}) {
    Object.assign(this,{
      arr: []
    }, options);
  }

  put(key, value) {
    this.arr.forEach(item => {
      if (item.key === key) {
        item.key.value = value;
      }
    });
    this.arr.push({
      key: key,
      value: value
    })
  }

  get(key) {
    for (let item of this.arr) {
      if (item.key === key) {
        return item.value;
      }
    }
    return null;
  }
}

export default HashMap;
